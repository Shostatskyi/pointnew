#include "PD.h"
#include "PP.h"
#include <iostream>
#include <cmath>

#define toDeg    180.0 / 3.141593
#define fromDeg  3.141593 / 180.0

PD::PD(PP const &p)
{
	x = p.getR() * cos(p.getT() * fromDeg);
	y = p.getR() * sin(p.getT() * fromDeg);
}

PD::operator PP()
{
	return PP(sqrt(x * x + y * y), atan(y / x) * toDeg);
}

PD& PD::operator = (const PP &p)
{
	x = p.getR() * cos(p.getT() * fromDeg);
	y = p.getR() * sin(p.getT() * fromDeg);
	return *this;
}
