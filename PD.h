#pragma once
#include <iostream> 
using namespace std;

class PP;

class PD {
private:
	double x;
	double y;

public:
	PD() : x(0), y(0) {}
	PD(double x, double y) : x(x), y(y) {}
	PD(PD const &d) : x(d.x), y(d.y) {}
	PD(PP const &p);

	operator PP();
	PD& operator = (const PP &p);

	double getX() const { return x; }
	double getY() const { return y; }

	void display() const { cout << "x = " << x << ", y = " << y << endl; }
};