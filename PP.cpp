#include "PD.h"
#include "PP.h"
#include <iostream>
#include <cmath>

#define toDeg    180.0 / 3.141593
#define fromDeg  3.141593 / 180.0

PP::PP(PD const &d)
{
     r = sqrt(d.getX()  * d.getX() + d.getY() * d.getY());
	 t = atan(d.getY() / d.getX()) * toDeg;
}

PP::operator PD()
{
	return PD(r * cos(t * fromDeg), r * sin(t * fromDeg));
}

PP& PP::operator = (const PD &d)
{
	r = sqrt(d.getX()  * d.getX() + d.getY() * d.getY());
	t = atan(d.getY() / d.getX()) * toDeg;
	return *this;
}