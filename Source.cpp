#include "PP.h"
#include "PD.h"
#include <iostream>
using namespace std;

void main() 
{
	// constructor 
	PD d1(12, 5); 
	d1.display();

	PP p1(d1);
	p1.display();

	PD d2(p1);
	d2.display();

	// operator = and operator of type converssion
	PP p2 = d1;
	p2.display();

	PD d3 = p2;
	d3.display();

	system("pause");
}