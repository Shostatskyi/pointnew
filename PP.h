#pragma once

#include <iostream> 
using namespace std;

class PD;

class PP {
private:
	double r;
	double t;

public:
	PP() : r(0), t(0) {}
	PP(double r, double t) : r(r), t(t) {}
	PP(PP const &p) : r(p.r), t(p.t) {}
	PP(PD const &d);

	operator PD();
	PP& operator = (const PD &d);

	double getR() const { return r; }
	double getT() const { return t; }

	void display() const { cout << "r = " << r << ", t = " << t << endl; }
};
